#!/bin/bash

set -e

work_arch_packages()
{
    echo '# Installing Arch Packages'
    pacman -S --needed --noconfirm \
        aws-cli-v2 \
        globalprotect-openconnect \
        libaio \
        php \
        subversion \
        unzip \
        zip
}

work_dns()
{
    doas tee /etc/resolv.conf.gsvpn > /dev/null <<EOF
search georgiasouthern.edu lan
nameserver 141.165.6.6
nameserver 141.165.1.9
EOF
}

work_oraic()
{
    echo '# Installing Oracle InstantClient'
    if [[ ! -d /opt/oracle ]]; then
        doas mkdir /opt/oracle
    fi
    if [[ ! -d /opt/oracle/instantclient_21_11 ]]; then
        filename=oraic.zip
        curl -sSL -o $filename \
            'https://download.oracle.com/otn_software/linux/instantclient/2111000/instantclient-basiclite-linux.x64-21.11.0.0.0dbru.zip'
        doas unzip $filename -d /opt/oracle
        rm $filename
    fi
    if [[ ! -L /opt/oracle/instantclient ]]; then
        doas ln -sf /opt/oracle/instantclient_21_11 /opt/oracle/instantclient
    fi
    if [[ ! -f /etc/ld.so.conf.d/oraic.conf ]]; then
        echo /opt/oracle/instantclient | \
            doas tee /etc/ld.so.conf.d/oraic.conf > /dev/null
        ldconfig
    fi
}

work_python_packages()
{
    echo '# Installing Python Packages'
}

work_sdkman()
{
    echo '# Installing SDKMAN'
    curl -s "https://get.sdkman.io?rcupdate=false" | bash
}


if [[ "$1" == '-h' ]]; then
    cat <<EOT
USAGE:
    $0 <function>
FUNCTIONS:
EOT
    declare -F | awk {'print "    "$3'}
    exit
fi

if echo "$@" | grep -iq '^work_' ; then
    eval "$@" | tee -a $0.log
else
    echo "invalid function: $@"
fi
