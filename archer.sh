#!/bin/bash

set -e

# iwctl station <ifname> connect <ssid>
# systemctl stop reflector.service
#
# preinstall
# install
# reboot
#
# systemclt enable NetworkManager.service
# systemclt start NetworkManager.service
# nmcli --ask dev wifi connect $ssid
# postinstall

disk='/dev/nvme0n1'
partition_boot=${disk}p1
partition_swap=${disk}p2
partition_root=${disk}p3
partition_boot_size='+1G'
partition_swap_size='+32G'
partition_root_size='0'

boot_dir='/boot'
bootloader='systemd'
charset='UTF-8'
console_font='ter-132n'
display_server='wayland'
firmware='uefi'
hostname=''
keymap='us'
locale='en_US.UTF-8'
root_passwd=''
ssd_trim='true'
timezone='/usr/share/zoneinfo/America/New_York'
ucode=''
username=''
username_passwd=''


preinstall()
{
    echo '# Running Preinstall'

    preinstall_init
    preinstall_partition
}

preinstall_init()
{
    loadkeys $keymap
    setfont $console_font
    timedatectl set-ntp true
    reflector --latest 5 --country US --protocol https --sort score \
        --save /etc/pacman.d/mirrorlist
}

preinstall_partition()
{
    echo "# Partition and format $disk"

    # create partitions
    sgdisk -Z $disk
    sgdisk -n 1:0:$partition_boot_size -t 1:ef00 -c 1:ESP $disk
    sgdisk -n 2:0:$partition_swap_size -t 2:8200 -c 2:SWAP $disk
    sgdisk -n 3:0:$partition_root_size -t 3:8300 -c 3:ROOT $disk

    # formtat partitions
    mkfs.fat -F 32 -n BOOT $partition_boot
    mkswap -L SWAP $partition_swap
    mkfs.ext4 -L ROOT $partition_root

    # mount filesystems
    mount $partition_root /mnt

    mkdir /mnt/boot
    mount $partition_boot /mnt/boot

    swapon $partition_swap
}

install()
{
    echo '# Running Install'

    install_pacstrap
    install_configure
}

install_pacstrap()
{
    pacstrap /mnt \
        base base-devel \
        linux linux-lts linux-firmware \
        networkmanager \
        opendoas \
        $ucode \
        neovim
}

install_configure()
{
    # Fstab
    genfstab -U /mnt > /mnt/etc/fstab

    # Timezone
    ln -sf $timezone /mnt/etc/localtime

    # Run hwclock(8) to generate /etc/adjtime
    arch-chroot /mnt hwclock --systohc

    # Generate locales
    sed -i -e "s/#$locale $charset/$locale $charset/" /mnt/etc/locale.gen
    arch-chroot /mnt locale-gen

    # Create /etc/locale.conf
    cat > /mnt/etc/locale.conf <<EOF
LANG=$locale
LC_ADDRESS=$locale
LC_IDENTIFICATION=$locale
LC_MEASUREMENT=$locale
LC_MONETARY=$locale
LC_NAME=$locale
LC_NUMERIC=$locale
LC_PAPER=$locale
LC_TELEPHONE=$locale
LC_TIME=$locale
EOF

    # Create /etc/hostname
    echo $hostname > /mnt/etc/hostname

    # Create /etc/hosts
    cat > /mnt/etc/hosts <<EOF
127.0.0.1   localhost
::1         localhost
127.0.1.1   ${hostname}.local $hostname
EOF

    # Virtual console font
    cat > /mnt/etc/vconsole.conf <<EOF
KEYMAP=$keymap
#FONT=$console_font
EOF

    # Root password
    echo -e "${root_passwd}\n$root_passwd" | arch-chroot /mnt passwd

    # bootloader
    install_bootloader_$bootloader

    # SSD
    if [ -n "$ssd_trim" ]; then
        arch-chroot /mnt systemctl enable fstrim.timer
    fi

    # User
    arch-chroot /mnt useradd -m -s /bin/bash $username
    echo -e "${username_passwd}\n$username_passwd" | arch-chroot /mnt passwd $username
    arch-chroot /mnt usermod -a -G video,wheel $username

    # Priviledge elevation
    echo "permit persist $username" > /mnt/etc/doas.conf
    sed -i -e 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /mnt/etc/sudoers

    # Save for postinstall
    mv $0 /mnt/home/$username

    # El fin
    umount -R /mnt
    echo 'Done'
}

install_bootloader_grub()
{
    arch-chroot /mnt pacman -S efibootmgr grub

    # Install GRUB
    arch-chroot /mnt grub-install \
        --target=x86_64-efi \
        --bootloader-id=grub \
        --efi-directory=$boot_dir \
        --recheck

    # Update /etc/default/grub
    arch-chroot /mnt sed -i 's/GRUB_TIMEOUT=.*$/GRUB_TIMEOUT=-1/' /etc/default/grub
    arch-chroot /mnt sed -i 's/GRUB_DEFAULT=.*$/GRUB_DEFAULT=saved/' /etc/default/grub
    arch-chroot /mnt sed -i 's/#GRUB_SAVEDEFAULT=.*$/GRUB_SAVEDEFAULT=true/' /etc/default/grub
    arch-chroot /mnt sed -i -E '/^GRUB_CMDLINE_LINUX_DEFAULT=/ s/(\s)?quiet\s?/\1/' /etc/default/grub

    # Update config
    arch-chroot /mnt grub-mkconfig -o ${boot_dir}/grub/grub.cfg
}

install_bootloader_systemd()
{
    # Install systemd-boot to ESP
    arch-chroot /mnt bootctl install

    # Add esp/loader/entries/*.conf
    local root=$(blkid $partition_root -o export | grep ^UUID)

    cat > /mnt/boot/loader/entries/arch.conf <<EOF
title Arch Linux
linux /vmlinuz-linux
initrd /${ucode}.img
initrd /initramfs-linux.img
options root=$root
EOF

    cat > /mnt/boot/loader/entries/arch-lts.conf <<EOF
title Arch Linux LTS
linux /vmlinuz-linux-lts
initrd /${ucode}.img
initrd /initramfs-linux-lts.img
options root=$root
EOF
}

postinstall()
{
    echo '# Running Postinstall'

    postinstall_packages_common
    postinstall_packages_$display_server
}

postinstall_packages_common()
{
    pacman -S --needed \
        alacritty alsa-utils \
        bat bind blueman \
        dhclient dmidecode docker docker-compose dosfstools \
        efibootmgr exa \
        firefox fwupd fzf \
        gawk geany git gnome-keyring go gptfdisk \
        hunspell-en_us \
        man-db man-pages \
        nfs-utils nnn ntfs-3g nuspell \
        networkmanager networkmanager-openconnect networkmanager-openvpn \
        nm-connection-editor noto-fonts-emoji \
        openconnect openssh openvpn \
        pipewire-pulse python python-pip python-pipx \
        ripgrep rustup \
        starship \
        tlp ttf-dejavu ttf-jetbrains-mono-nerd \
        udiskie udisks2 \
        vi \
        xdg-user-dirs
}

postinstall_packages_extra()
{
    pacman -S --needed \
        # boost \
        # check clang \
        # delve \
        # ebtables edk2-ovmf edk2-armvirt \
        # fd \
        # jq \
        # inetutils \
        # procs \
        # qemu-desktop qemu-emulators-full \
        # rsync
        # sqlite3
        # virt-manager
}

postinstall_packages_wayland()
{
    pacman -S --needed \
        bemenu-wayland \
        gammastep glfw-wayland grim \
        imv \
        light \
        mako mpv \
        nnn \
        qt5-wayland qt6-wayland \
        slurp sway swaybg swayidle swaylock \
        waybar wayland wl-clipboard \
        xdg-desktop-portal-wlr \
        xorg-xlsclients xorg-xwayland
}

postinstall_packages_x11()
{
    pacman -S --needed \
        bemenu bmenu-x11 bluez bluez-utils \
        dunst \
        gcolor2 \
        hacksaw \
        i3-wm i3status imv \
        light \
        nnn network-manager-applet \
        redshift \
        shotgun slock \
        xorg xorg-drivers xorg-xinit \
        xcmenu xcursor-breeze xdg-utils xiccd xterm \
        zoom
}



if [[ "$1" == '-h' ]]; then
    cat <<EOT
USAGE:
    $0 <function>
FUNCTIONS:
EOT
    declare -F | awk {'print "    "$3'}
    exit
fi

eval "$@" | tee -a $0.log
