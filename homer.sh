#!/bin/bash

set -e

alacritty_colors()
{
    echo Installing Alacritty colorschemes
    if [[ -e ~/.config/alacritty/colors/aarowill ]]; then
        echo already installed
        return
    fi
    git clone --depth 1 https://github.com/aarowill/base16-alacritty.git \
        ~/.config/alacritty/colors/aarowill
    # git clone --depth 1 https://github.com/eendroroy/alacritty-theme.git \
        # ~/.config/alacritty/colors/eendroroy
}

dns()
{
    echo Updating resolv.conf
    if [[ -L /etc/resolv.conf ]]; then
        echo already setup
        return
    fi
    doas chown :wheel /etc && doas chmod g+w /etc
    if [[ -f /etc/resolv.conf && ! -f /etc/resolv.conf.save ]]; then
        doas mv /etc/resolv.conf /etc/resolv.conf.save
    fi
    doas tee /etc/NetworkManager/conf.d/90-dns.conf > /dev/null <<EOF
[main]
dns=none
EOF
    doas tee /etc/resolv.conf.manual > /dev/null <<EOF
nameserver 1.1.1.1
nameserver 9.9.9.9
EOF
    doas ln -sf /etc/resolv.conf.manual /etc/resolv.conf
}

dotfiles()
{
    echo Installing dotfiles
    if [[ -e ~/.dotfiles ]]; then
        echo already installed
        return
    fi
    git clone --bare https://gitlab.com/jcoquinn/dotfiles.git ~/.dotfiles
    git --git-dir="$HOME/.dotfiles" --work-tree="$HOME" checkout -f
    [ -f ~/.aliases ] && . ~/.aliases
    [ -f ~/.functions ] && . ~/.functions
    dofi config --local status.showUntrackedFiles no
}

go_packages()
{
    echo Installing Go Packages
    declare -A pkgs
    pkgs=(
        [air]=github.com/cosmtrek/air@latest
        [dlv]=github.com/go-delve/delve/cmd/dlv@latest
        [gofumpt]=mvdan.cc/gofumpt@latest
        [goimports]=golang.org/x/tools/cmd/goimports@latest
        [golines]=github.com/segmentio/golines@latest
        [gopls]=golang.org/x/tools/gopls@latest
        [staticcheck]=honnef.co/go/tools/cmd/staticcheck@latest
    )
    gopath=$(go env GOPATH)
    for k in "${!pkgs[@]}"; do
        if [[ -e $gopath/bin/$k ]]; then
            echo $k already installed
            continue
        fi
        echo "${pkgs[$k]}..."
        go install ${pkgs[$k]}
    done
}


ohmybash()
{
    echo Installing oh-my-bash
    if [[ -e ~/.oh-my-bash ]]; then
        echo already installed
    else
        local url='https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh'
        bash -c "$(curl -fsSL $url)"
    fi

    echo Installing .oh-my-bash/custom
    if [[ -e ~/.oh-my-bash/custom/rc.sh ]]; then
        echo already installed
    else
        rm -rf ~/.oh-my-bash/custom
        git clone --depth 1 https://gitlab.com/jcoquinn/oh-my-bash.git \
            ~/.oh-my-bash/custom
    fi
}

mkdirs()
{
    mkdir -p \
        ~/bin \
        ~/projects/me \
        ~/projects/work
}

nvim()
{
    echo Installing .nvim/config
    if [[ -e ~/.config/nvim ]]; then
        echo already installed
    else
        git clone --depth 1 https://gitlab.com/jcoquinn/nvim-config.git \
            ~/.config/nvim
    fi
}

_nvm_init()
{
    if command -v nvm; then
        return
    elif [ -s "$NVM_DIR/nvm.sh" ]; then
        echo Activating nvm
        . "$NVM_DIR/nvm.sh"
        return
    fi
    echo Installing nvm
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash
}

node_install()
{
    _nvm_init
    nvm install "$@"
}

node_packages()
{
    _nvm_init
    if ! nvm use --lts; then
        node_install --lts
    fi

    echo Installing Node packages
    local pkgs=(
        prettier
    )
    for p in "${pkgs[@]}"; do
        if [[ -e "$NVM_BIN/$p" ]]; then
            echo $p already installed
        else
            npm i -g $p
        fi
    done
}

pyenv()
{
    echo Installing pyenv
    if [[ -e "$PYENV_ROOT" ]]; then
        echo already installed
        return
    fi
    curl https://pyenv.run | bash
}

python_packages()
{
    echo Installing Python Packages
    local pkgs=(
        bashate
        flake8
        pre-commit
        python-lsp-server
        yamllint
    )
    for p in "${pkgs[@]}"; do
        if [[ -e ~/.local/bin/$p ]] || [[ -e ~/.local/pipx/venvs/$p ]]; then
            pipx reinstall $p
        else
            pipx install $p
        fi
    done
    if [[ -e ~/.local/pipx/venvs/python-lsp-server ]]; then
        pipx inject python-lsp-server python-lsp-black
        pipx inject python-lsp-server python-lsp-isort
        pipx inject python-lsp-server pylsp-mypy
    fi
}

rust_cargos()
{
    echo Installing Cargos
    if rustup toolchain list | grep -iq '^no'; then
        rustup default stable
    fi

    local cargos=(
        stylua
    )
    for c in "${cargos[@]}"; do
        if [[ -e ~/.cargo/bin/$c ]]; then
            echo $c already installed
            continue
        fi
        echo $c...
        cargo install $c
    done
}

sdkman()
{
    echo Installing sdkman
    if [[ -e "$SDKMAN_DIR" ]]; then
        echo already installed
        return
    fi
    curl -s https://get.sdkman.io | bash
}

yay()
{
    echo Installing yay
    if [[ -f /usr/bin/yay ]]; then
        echo already installed
        return
    fi
    git clone --depth 1 https://aur.archlinux.org/yay-bin.git
    cd yay-bin
    makepkg -si
    rm -rf yay-bin
}


if [[ "$1" == '-h' ]]; then
    cat <<EOT
USAGE:
    $0 <function>
FUNCTIONS:
EOT
    declare -F | awk {'print "    "$3'}
    exit
fi

eval "$@" | tee -a $0.log